package com.p79.plain.tesprimefaces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesprimefacesApplication {

	public static void main(String[] args) {
		System.out.println("TES");
		SpringApplication.run(TesprimefacesApplication.class, args);

	}

}
